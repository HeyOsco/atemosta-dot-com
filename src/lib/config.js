const PAGES = [

  // {"name": "The Grand Challenge", "desc": "Share thoughts and sensations with your virtual reality avatar as you struggle to survive in this online interactive experience", "link": "https://tgc.atos.to/"},
  {
    "name": "Sway XR", 
    "desc": "An app to learn any dance at your own pace with mixed reality and AI", 
    "link": "https://sway.quest/"
  },
  {"name": "Socials", "desc": "Our rendezvous across the metaverse", "link": "/socials"},
  {"name": "Contact", "desc": "For business inquiries or general questions", "link": "/contact"},
]
const PAGES_OLD = [
  {"name": "Contact", "desc": "For business inquiries or general questions", "link": "/contact"},
  {"name": "Lore", "desc": "Publishing site for creative writing works", "link": "https://lore.atemosta.com/"},
  {"name": "Immria", "desc": "Create, customize, and hangout in virtual reality worlds within your web browser", "link": "https://www.immria.com/"},
  {"name": "Myujen", "desc": "Create and share assets for virtual reality experiences by using prompts", "link": "https://www.myujen.com/"},  
  {"name": "PP_ESports", "desc": "Adult E-Sports Leagues for your local community", "link": "https://ppesports.pro/"},
  {"name": "Socials", "desc": "Our rendezvous across the metaverse", "link": "/socials"},
  {"name": "Subscribe", "desc": "Support our work and get exclusive rewards!", "link": "/subscribe"},
  {"name": "Team", "desc": "Who we are", "link": "/team"}
]

const SITE_ABOUT = "We are a startup studio creating virtual reality experiences"
const SITE_NAME = "Atemosta"
const SITE_TITLE = "Atemosta"
const SOCIALS = [
  // {
  //   "name": "Discord",
  //   "desc": "osco",
  //   "link": "https://discord.gg"
  // },
  // {   
  //   "name": "Instagram", 
  //   "desc": "oscoDOTblog", 
  //   "link": "https://www.instagram.com/oscodotblog/"
  // }
  // {
  //   "name": "Keybase", 
  //   "desc": "@Atemosta", 
  //   "link": "https://keybase.io/Atemosta"
  // },
  // {   
  //   "name": "LunkedIn", 
  //   "desc": "oscoDOTblog", 
  //   "link": "https://www.linkedin.com/in/oscodotblog/"
  // },
  {
    "name": "Mastodon", 
    "desc": "@Atemosta@mastodon.social", 
    "link": "https://mastodon.social/@Atemosta"
  },
  {
    "name": "Odysee", 
    "desc": "@Atemosta", 
    "link": "https://odysee.com/@Atemosta"
  },
  // {
  //   "name": "Revolt", 
  //   "desc": "Open-source messaging platform (Discord alternative)", 
  //   "link": "https://app.revolt.chat/invite/S2H3wGSq"
  // },
  {
    "name": "Twitter (X)", 
    "desc": "@Atemosta", 
    "link": "https://twitter.com/atemosta"
  },
  {
    "name": "YouTube", 
    "desc": "@Atemosta", 
    "link": "https://www.youtube.com/@Atemosta"
  },
]
const TEAM = [
  {"name": "Oscar Kevin", "desc": "Software Developer and Virtual Reality Architect", "link": "https://www.linkedin.com/in/oscarkevin", "role": "Founder/CTO"},
  {"name": "Ousikai", "desc": "Author and curator for Atemosta's creative works", "link": "https://mastodon.social/@Ousikai", "role": "Founder/Creative Director"},
]
const TEAM_IMAGES = {
  "Oscar Kevin": "/images/atos-osco.png",
  "Ousikai": "/images/atos-ousi.png"
}
export {
  PAGES,
  SITE_ABOUT,
  SITE_NAME,
  SITE_TITLE,
  SOCIALS,
  TEAM,
  TEAM_IMAGES
}
