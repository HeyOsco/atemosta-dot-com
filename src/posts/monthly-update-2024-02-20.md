---
title: 'The Year of the Grand Challenge 🐉'
date: '2024-02-20'
---
Happy Lunar New Year 2024! 🐉

***Here is our roadmap for this year...***

## The Grand Challenge
Our primary focus this year will be releasing the *first arc of **The Grand Challenge***! 

This is an interactive story where the player shares their thoughts and sensations with their virtual reality avatar as they try to survive together in a post-apocalyptic world.

The current story can be experienced at [https://tgc.atos.to](https://tgc.atos.to)

The story is loosely based on the ongoing short story located at [https://lore.atemosta.com/blog/the-grand-challenge](https://lore.atemosta.com/blog/the-grand-challenge)

We are shooting for one episode a month, so stay tuned for the episode 2 release sometime early March!

## Immria
We are exploring the use of [Mozilla Hubs](https://github.com/mozilla/hubs-cloud/tree/master/community-edition) to deploy virtual spaces we will use for special events and as [Patreon Rewards](https://osco.blog/subscribe). 

If there is sufficient demand, we will make the online worlds we create available for private use at a fair price~

Stay tuned for more: [https://immria.com/](https://immria.com/)

## Myujen
Myujen is the companion app for creating experiences like **The Grand Challenge**! 

Though long deprecated, we looking to:
* Update the UX of the site
* Add the new style options for creating 360 degree skyboxes
* Create full-body character illustrations using text-to-image stable diffusion models
* Release a *version one* of creating virtual experiences from just the website alone!

Stay tuned for more: [https://myujen.com/](https://myujen.com/)

## Conclusion

And that's it for this week! Thanks for reading, and stay tuned for fun things coming soon from Atemosta!

![FEH Azura](/images/feh-azura.png)