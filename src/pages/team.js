import Head from 'next/head';
import Image from 'next/image';
import Layout from '../components/layout';
import Link from 'next/link';
import { TEAM, TEAM_IMAGES } from '../lib/config';
import utilStyles from '../styles/utils.module.css';

const pageName = "Team"

export default function Team() {
    return (
      <Layout>        
        <Head>
          <title>{pageName}</title>
        </Head>
        <h1>{pageName}</h1>
        <section className={utilStyles.headingMd}>
        {TEAM.map(({name, desc, link, role}) => (
          <div key={name}>
            <Image
              priority
              src={TEAM_IMAGES[name]}
              className={utilStyles.borderCircle}
              height={144}
              width={144}
              alt=""
            />
            <h3>{name}</h3>
            <h4><Link href={link}>{role}</Link></h4>
            <p>{desc}</p>
          </div>
        ))}
      </section>
      </Layout>
    );
  }
  