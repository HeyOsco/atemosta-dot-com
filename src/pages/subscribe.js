import Link from 'next/link';
import Head from 'next/head';
import Layout from '../components/layout';


export default function Donate() {
    return (
      <Layout>        
        <Head>
          <title>Donate</title>
        </Head>
        <h1>Donate</h1>
        <h2>
          <Link href="/">← Back to home</Link>
        </h2>
      </Layout>
    );
  }
  