import Head from 'next/head';
import Layout from '../components/layout';
import Link from 'next/link';
import { SOCIALS } from '../lib/config';
import utilStyles from '../styles/utils.module.css';

const pageName = "Socials"

export default function Socials() {
    return (
      <Layout>        
        <Head>
          <title>{pageName}</title>
        </Head>
        <h1>{pageName}</h1>
        <section className={utilStyles.headingMd}>
        {SOCIALS.map(({name, desc, link}) => (
          <p key={name}>
            <Link href={link}>{name}</Link> - {desc}
          </p>
        ))}
      </section>
      </Layout>
    );
  }
  