import Head from 'next/head';
import Layout from '../components/layout';
const pageName = "Contact"
export default function Contact() {
    return (
<div>
      <Layout>  
        <Head>
          <title>{pageName}</title>
        </Head>
        <h1>{pageName}</h1>
        <p>For business inquiries, please email <b><i>ohaiyo+business@atemosta.com</i></b></p>
        <p>For general questions, please email <b><i>ohaiyo+contact@atemosta.com</i></b></p>
      </Layout>
      </div>
    );
  }
  