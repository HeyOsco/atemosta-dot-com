// NextJS Imports
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
// Config Imports
import { PAGES, SITE_ABOUT, SITE_TITLE } from '../lib/config';
// Component Imports
import Date from '../components/date';
import Layout, { siteTitle } from '../components/layout';
// Library Imports
import { getSortedPostsData } from '../lib/posts';
// Styling Imports
import { bgWrap } from '../styles/bg.module.css'
import utilStyles from '../styles/utils.module.css';


export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Home({ allPostsData }) {
  return (
    <div>
      <Layout home>
      {/* <Image
        alt="background"
        src="/bg/parasol.jpg"
        layout="fill"
        objectFit="cover"
        quality={100}
      /> */}
        <Head>
          <title>{siteTitle}</title>
          {/* Plausible Analytics */}
          <script defer data-domain="atemosta.com" src="https://plausible.atemosta.com/js/script.js"></script>
        </Head>
        <section className={utilStyles.headingMd}>
          <h3>Welcome to <i>{SITE_TITLE}</i></h3>
          <h4>{SITE_ABOUT} founded by <Link href="https://www.osco.blog/">{"oscoDOTblog"}</Link></h4>
          {PAGES.map(({name, desc, link}) => (
            <p key={name}>
              <Link href={link}>{name}</Link> - {desc}
            </p>
          ))}
        </section>

        {/* Monthly Updates */}
        {/* <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>Monthly Updates</h2>
          <ul className={utilStyles.list}>
            {allPostsData.map(({ id, date, title }) => (
              <li className={utilStyles.listItem} key={id}>
                <Link href={`/posts/${id}`}>{title}</Link>
                <br />
                <small className={utilStyles.lightText}>
                  <Date dateString={date} />
                </small>
              </li>
            ))}
          </ul>
        </section> */}

      </Layout>
    </div>
  );
}
